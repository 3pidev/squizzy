module.exports = {
  sanityClientConfig: {
    //apiHost: "http://localhost:3900",
    // replace this id with the one you find in /studio/sanity.json
    projectId: 'pv5f4dow-x',
    // make sure that the dataset name is the same as the one in /studio/sanity.json
    dataset: 'production',
    namespace: 'production',
    useProjectHostname: true,
    gradientMode: true,
    useCdn: false
  }
}
